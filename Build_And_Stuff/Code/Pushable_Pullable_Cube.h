// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "Pushable_Pullable_Cube.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnActorInteractable, bool, bIsInRage);

UCLASS()
class ST_PUSHPULL_API APushable_Pullable_Cube : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APushable_Pullable_Cube();

	FOnActorInteractable OnActorInteractable;

 
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UStaticMeshComponent* CubeStaticMesh = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UStaticMeshComponent* Handle1 = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UStaticMeshComponent* Handle2 = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UStaticMeshComponent* Handle3 = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UStaticMeshComponent* Handle4 = nullptr;
	
	//
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UBoxComponent* CollisionBoxXP = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UBoxComponent* CollisionBoxXN = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UBoxComponent* CollisionBoxYP = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UBoxComponent* CollisionBoxYN = nullptr;
	
	//
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UArrowComponent* Arrow1 = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UArrowComponent* Arrow2 = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UArrowComponent* Arrow3 = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
		class UArrowComponent* Arrow4 = nullptr;

	//**
	UPROPERTY(BlueprintReadWrite, Category = Variables)
		bool HaveActorInInteractionZone = false;

	UPROPERTY(BlueprintReadWrite, Category = Variables)
		bool IsInteracting = false;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//**
	UFUNCTION()
		void CollisionBoxXPBeginOverlap(class UPrimitiveComponent* OverlappedComponent,
										AActor* OtherActor,
										UPrimitiveComponent* OtherComponent,
										int32 OtherBodyIndex,
										bool bFromSweep,
										const FHitResult& SweepResult);
	UFUNCTION()
		void CollisionBoxXPEndOverlap (class UPrimitiveComponent* OverlappedComp,
										class AActor* OtherActor,
										class UPrimitiveComponent* OtherComp,
										int32 OtherBodyIndex);

	//**
	UFUNCTION()
		void CollisionBoxXNBeginOverlap(class UPrimitiveComponent* OverlappedComponent,
										AActor* OtherActor,
										UPrimitiveComponent* OtherComponent,
										int32 OtherBodyIndex,
										bool bFromSweep,
										const FHitResult& SweepResult);

	UFUNCTION()
		void CollisionBoxXNEndOverlap (class UPrimitiveComponent* OverlappedComp,
										class AActor* OtherActor,
										class UPrimitiveComponent* OtherComp,
										int32 OtherBodyIndex);

	//**

	UFUNCTION()
		void CollisionBoxYPBeginOverlap(class UPrimitiveComponent* OverlappedComponent,
										AActor* OtherActor,
										UPrimitiveComponent* OtherComponent,
										int32 OtherBodyIndex,
										bool bFromSweep,
										const FHitResult& SweepResult);

	UFUNCTION()
		void CollisionBoxYPEndOverlap (class UPrimitiveComponent* OverlappedComp,
										class AActor* OtherActor,
										class UPrimitiveComponent* OtherComp,
										int32 OtherBodyIndex);

	//**

	UFUNCTION()
		void CollisionBoxYNBeginOverlap(class UPrimitiveComponent* OverlappedComponent,
										AActor* OtherActor,
										UPrimitiveComponent* OtherComponent,
										int32 OtherBodyIndex,
										bool bFromSweep,
										const FHitResult& SweepResult);

	UFUNCTION()
		void CollisionBoxYNEndOverlap (class UPrimitiveComponent* OverlappedComp,
										class AActor* OtherActor,
										class UPrimitiveComponent* OtherComp,
										int32 OtherBodyIndex);

	void AddOrientedLocalcOffset(FVector ForwardVector);
};
