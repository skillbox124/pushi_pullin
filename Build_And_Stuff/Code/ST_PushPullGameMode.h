// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ST_PushPullGameMode.generated.h"

UCLASS(minimalapi)
class AST_PushPullGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AST_PushPullGameMode();
};



