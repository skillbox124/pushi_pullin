// Fill out your copyright notice in the Description page of Project Settings.
#include "Pushable_Pullable_Cube.h"

#include "../ST_PushPull/ST_PushPullCharacter.h"


// Sets default values
APushable_Pullable_Cube::APushable_Pullable_Cube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;;
	//
	CubeStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CubeMesh"));
	CubeStaticMesh->SetWorldRotation({ 0.0f,0.0f,0.0f });
	CubeStaticMesh->SetSimulatePhysics(false);
	//CubeStaticMesh->SetMassOverrideInKg(NAME_None, 50.0f, true);
	CubeStaticMesh->SetLinearDamping(2.0f);
	CubeStaticMesh->SetEnableGravity(true);
	CubeStaticMesh->SetConstraintMode(EDOFMode::XYPlane);
	RootComponent = CubeStaticMesh;


	///////Handles Meshes//////
	Handle1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Handle X positive"));
	Handle1->SetupAttachment(CubeStaticMesh);
	//
	Handle2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Handle X negative"));
	Handle2->SetupAttachment(CubeStaticMesh);
	//
	Handle3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Handle Y positive"));
	Handle3->SetupAttachment(CubeStaticMesh);
	//
	Handle4 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Handle Y negative"));
	Handle4->SetupAttachment(CubeStaticMesh);

	
	///////Collisions///////
	CollisionBoxXP = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Check Box positive X"));
	CollisionBoxXP->SetupAttachment(Handle1);
	//XCollisionBox1->OnComponentBeginOverlap();
	//
	CollisionBoxXN = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Check Box negative X"));
	CollisionBoxXN->SetupAttachment(Handle2);
	//
	CollisionBoxYP = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Check Box positive Y"));
	CollisionBoxYP->SetupAttachment(Handle3);
	//
	CollisionBoxYN = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Check Box negative Y"));
	CollisionBoxYN->SetupAttachment(Handle4);


	////////ArrowComponents//////
	Arrow1 = CreateDefaultSubobject<UArrowComponent>(TEXT("XBOX1 positive"));
	Arrow1->SetupAttachment(Handle1);
	//
	Arrow2 = CreateDefaultSubobject<UArrowComponent>(TEXT("XBOX2 negative"));
	Arrow2->SetupAttachment(Handle2);
	//
	Arrow3 = CreateDefaultSubobject<UArrowComponent>(TEXT("YBOX1 positive"));
	Arrow3->SetupAttachment(Handle3);
	//
	Arrow4 = CreateDefaultSubobject<UArrowComponent>(TEXT("YBOX2 negative"));
	Arrow4->SetupAttachment(Handle4);
}


// Called when the game starts or when spawned
void APushable_Pullable_Cube::BeginPlay()
{
	Super::BeginPlay();
	
	//**
	CollisionBoxXP->OnComponentBeginOverlap.AddDynamic(this, &APushable_Pullable_Cube::CollisionBoxXPBeginOverlap);
	CollisionBoxXP->OnComponentEndOverlap.AddDynamic(this, &APushable_Pullable_Cube::CollisionBoxXPEndOverlap);
	
	//**
	CollisionBoxXN->OnComponentBeginOverlap.AddDynamic(this, &APushable_Pullable_Cube::CollisionBoxXNBeginOverlap);
	CollisionBoxXN->OnComponentEndOverlap.AddDynamic(this, &APushable_Pullable_Cube::CollisionBoxXNEndOverlap);

	//**
	CollisionBoxYP->OnComponentBeginOverlap.AddDynamic(this, &APushable_Pullable_Cube::CollisionBoxYPBeginOverlap);
	CollisionBoxYP->OnComponentEndOverlap.AddDynamic(this, &APushable_Pullable_Cube::CollisionBoxYPEndOverlap);

	//**
	CollisionBoxYN->OnComponentBeginOverlap.AddDynamic(this, &APushable_Pullable_Cube::CollisionBoxYNBeginOverlap);
	CollisionBoxYN->OnComponentEndOverlap.AddDynamic(this, &APushable_Pullable_Cube::CollisionBoxYNEndOverlap);
}

// Called every frame
void APushable_Pullable_Cube::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APushable_Pullable_Cube::CollisionBoxXPBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	HaveActorInInteractionZone = true;

	FVector HandleForwardLocation;
	FRotator HandleForwardRotation;
	FVector HandleForwardVector;
	FVector HandleForwardLocationCorrection;

	//**Location to which character needs to move for Animation
	HandleForwardLocation = Arrow1->GetComponentLocation();

	//**Rotation for character to look at
	HandleForwardRotation = Arrow1->GetComponentRotation();

	//** Forward vectors determines orientation movement of character and cube
	HandleForwardVector = Arrow1->GetForwardVector();

	//** Offset for character placement neat object for animation adjustments
	HandleForwardLocationCorrection = { 0.0f, 0.0f, 0.0f };

	//** Character reference 
	AST_PushPullCharacter* MyChar = Cast<AST_PushPullCharacter>(OtherActor);

	//** If conditions are met start moving and turning chacater closer to object 
	if (OtherActor == MyChar && MyChar != nullptr)
	{
		MyChar->SetFinalCharLocationAndRotation(HandleForwardLocation, HandleForwardRotation, HandleForwardVector, HandleForwardLocationCorrection);
		MyChar->IsInInteractionRangeOfObject = true;
	}
	UKismetSystemLibrary::PrintString(GetWorld(), "Cube->Char_Begin::SetFinalCharLocAndRot", true, true, FLinearColor::Blue, 2.0f);
	
}

void APushable_Pullable_Cube::CollisionBoxXPEndOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	HaveActorInInteractionZone = false;
	AST_PushPullCharacter* MyChar = Cast<AST_PushPullCharacter>(OtherActor);
	if (OtherActor == MyChar && MyChar != nullptr)
	{
		MyChar->IsInInteractionRangeOfObject = false;
		FVector VZero = { 0.0f, 0.0f, 0.0f };
		FRotator RZero = { 0.0f, 0.0f, 0.0f };
		MyChar->SetFinalCharLocationAndRotation(VZero, RZero, VZero, VZero);
	}
	UKismetSystemLibrary::PrintString(GetWorld(), "Cube->Char_End::SetFinalCharLocAndRot", true, true, FLinearColor::Blue, 2.0f);
}

//**
void APushable_Pullable_Cube::CollisionBoxXNBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	HaveActorInInteractionZone = true;

	FVector HandleForwardLocation;
	FRotator HandleForwardRotation;
	FVector HandleForwardVector;
	FVector HandleForwardLocationCorrection;

	//**Location to which character needs to move for Animation
	HandleForwardLocation = Arrow2->GetComponentLocation();

	//**Rotation for character to look at
	HandleForwardRotation = Arrow2->GetComponentRotation();

	//** Forward vectors determines orientation movement of character and cube
	HandleForwardVector = Arrow2->GetForwardVector();

	//** Offset for character placement neat object for animation adjustments
	HandleForwardLocationCorrection = { 0.0f, 0.0f, 0.0f };

	//** Character reference 
	AST_PushPullCharacter* MyChar = Cast<AST_PushPullCharacter>(OtherActor);

	//** If conditions are met start moving and turning chacater closer to object 
	if (OtherActor == MyChar && MyChar != nullptr)
	{
		MyChar->SetFinalCharLocationAndRotation(HandleForwardLocation, HandleForwardRotation, HandleForwardVector, HandleForwardLocationCorrection);
		MyChar->IsInInteractionRangeOfObject = true;
		UKismetSystemLibrary::PrintString(GetWorld(), "Cube->Char_Begin::SetFinalCharLocAndRot", true, true, FLinearColor::Red, 2.0f);
	}
}

void APushable_Pullable_Cube::CollisionBoxXNEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	HaveActorInInteractionZone = false;
	AST_PushPullCharacter* MyChar = Cast<AST_PushPullCharacter>(OtherActor);
	if (OtherActor == MyChar && MyChar != nullptr)
	{
		MyChar->IsInInteractionRangeOfObject = false;
		FVector VZero = { 0.0f, 0.0f, 0.0f };
		FRotator RZero = { 0.0f, 0.0f, 0.0f };
		MyChar->SetFinalCharLocationAndRotation(VZero, RZero, VZero, VZero);
		UKismetSystemLibrary::PrintString(GetWorld(), "Cube->Char_End::SetFinalCharLocAndRot", true, true, FLinearColor::Red, 2.0f);
	}
}

//**
void APushable_Pullable_Cube::CollisionBoxYPBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	HaveActorInInteractionZone = true;

	FVector HandleForwardLocation;
	FRotator HandleForwardRotation;
	FVector HandleForwardVector;
	FVector HandleForwardLocationCorrection;

	//**Location to which character needs to move for Animation
	HandleForwardLocation = Arrow3->GetComponentLocation();

	//**Rotation for character to look at
	HandleForwardRotation = Arrow3->GetComponentRotation();

	//** Forward vectors determines orientation movement of character and cube
	HandleForwardVector = Arrow3->GetForwardVector();

	//** Offset for character placement neat object for animation adjustments
	HandleForwardLocationCorrection = { 0.0f, 0.0f, 0.0f };

	//** Character reference 
	AST_PushPullCharacter* MyChar = Cast<AST_PushPullCharacter>(OtherActor);

	//** If conditions are met start moving and turning chacater closer to object 
	if (OtherActor == MyChar && MyChar != nullptr)
	{
		MyChar->SetFinalCharLocationAndRotation(HandleForwardLocation, HandleForwardRotation, HandleForwardVector, HandleForwardLocationCorrection);
		MyChar->IsInInteractionRangeOfObject = true;
	}
	UKismetSystemLibrary::PrintString(GetWorld(), "Cube->Char_Begin::SetFinalCharLocAndRot", true, true, FLinearColor::Yellow, 2.0f);
}

void APushable_Pullable_Cube::CollisionBoxYPEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	HaveActorInInteractionZone = false;
	AST_PushPullCharacter* MyChar = Cast<AST_PushPullCharacter>(OtherActor);
	if (OtherActor == MyChar && MyChar != nullptr)
	{
		MyChar->IsInInteractionRangeOfObject = false;
		FVector VZero = { 0.0f, 0.0f, 0.0f };
		FRotator RZero = { 0.0f, 0.0f, 0.0f };
		MyChar->SetFinalCharLocationAndRotation(VZero, RZero, VZero, VZero);
	}
	UKismetSystemLibrary::PrintString(GetWorld(), "Cube->Char_End::SetFinalCharLocAndRot", true, true, FLinearColor::Yellow, 2.0f);
}

//**
void APushable_Pullable_Cube::CollisionBoxYNBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	HaveActorInInteractionZone = true;

	FVector HandleForwardLocation;
	FRotator HandleForwardRotation;
	FVector HandleForwardVector;
	FVector HandleForwardLocationCorrection;

	//**Location to which character needs to move for Animation
	HandleForwardLocation = Arrow4->GetComponentLocation();

	//**Rotation for character to look at
	HandleForwardRotation = Arrow4->GetComponentRotation();

	//** Forward vectors determines orientation movement of character and cube
	HandleForwardVector = Arrow4->GetForwardVector();

	//** Offset for character placement neat object for animation adjustments
	HandleForwardLocationCorrection = { 0.0f, 0.0f, 0.0f };

	//** Character reference 
	AST_PushPullCharacter* MyChar = Cast<AST_PushPullCharacter>(OtherActor);

	//** If conditions are met start moving and turning chacater closer to object 

	if (OtherActor == MyChar && MyChar != nullptr)
	{
		MyChar->SetFinalCharLocationAndRotation(HandleForwardLocation, HandleForwardRotation, HandleForwardVector, HandleForwardLocationCorrection);
		MyChar->IsInInteractionRangeOfObject = true;
		UKismetSystemLibrary::PrintString(GetWorld(), "Cube->Char_Begin::SetFinalCharLocAndRot", true, true, FLinearColor::Black, 2.0f);
	}

}

void APushable_Pullable_Cube::CollisionBoxYNEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	HaveActorInInteractionZone = false;
	AST_PushPullCharacter* MyChar = Cast<AST_PushPullCharacter>(OtherActor);
	if (OtherActor == MyChar && MyChar != nullptr)
	{
		MyChar->IsInInteractionRangeOfObject = false;
		FVector VZero = { 0.0f, 0.0f, 0.0f };
		FRotator RZero = { 0.0f, 0.0f, 0.0f };
		MyChar->SetFinalCharLocationAndRotation(VZero, RZero, VZero, VZero);
	}
	UKismetSystemLibrary::PrintString(GetWorld(), "Cube->Char_End::SetFinalCharLocAndRot", true, true, FLinearColor::Black, 2.0f);
}

//**
void APushable_Pullable_Cube::AddOrientedLocalcOffset(FVector ForwardVector)
{
	AddActorWorldOffset(ForwardVector,false,nullptr,ETeleportType::None);
}
