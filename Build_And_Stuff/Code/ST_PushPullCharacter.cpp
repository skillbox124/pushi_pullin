// Copyright Epic Games, Inc. All Rights Reserved.

#include "../ST_PushPull/ST_PushPullCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "DrawDebugHelpers.h"


#include "GameFramework/SpringArmComponent.h"

//////////////////////////////////////////////////////////////////////////
// AST_PushPullCharacter



AST_PushPullCharacter::AST_PushPullCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input
void AST_PushPullCharacter::Tick(float Deltatime)
{
	Super::Tick(Deltatime);
	
	/*
	if (IsInteractingWithObject)
	{
		CheckDistanceBetweenCharAndObj();
	}
	*/

	if (CharNeedsLocAndRotAdjustment)
	{
		InterpAlpha(TargetAlphaForLocAndRot, Deltatime);
	}


	if (Pushing || Pulling)
	{

		AddMovementInput(PushForward * ST_Direction, 0.5f);

		APushable_Pullable_Cube* Cube = Cast<APushable_Pullable_Cube>(UGameplayStatics::GetActorOfClass(GetWorld(),
																APushable_Pullable_Cube::StaticClass()));
		
		Cube->AddOrientedLocalcOffset(PushForward * ST_Direction * 0.5f);
	}
}

void AST_PushPullCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{

	//APushable_Pullable_Cube* TheCube = Cast <APushable_Pullable_Cube>));
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	//PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	//PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AST_PushPullCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AST_PushPullCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AST_PushPullCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AST_PushPullCharacter::LookUpAtRate);


	PlayerInputComponent->BindAction("Push", IE_Pressed,this, &AST_PushPullCharacter::Push);
	PlayerInputComponent->BindAction("Push", IE_Released, this, &AST_PushPullCharacter::StopPush);

	PlayerInputComponent->BindAction("Pull", IE_Pressed,this, &AST_PushPullCharacter::Pull);
	PlayerInputComponent->BindAction("Pull", IE_Released, this, &AST_PushPullCharacter::StopPull);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &AST_PushPullCharacter::Interact);
}

//**
void AST_PushPullCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AST_PushPullCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AST_PushPullCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f) && CanMove)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AST_PushPullCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) && CanMove)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

//**
void AST_PushPullCharacter::Push()
{


	if (!Pushing && !Pulling && bCan_Push_Pull)
	{
		StopPull();
		ST_Direction = 1.0f;
		Pushing = true;

	}
	if (Pulling)
	{
		//Pulling = false;
	}
}

void AST_PushPullCharacter::StopPush()
{
	if (Pushing)
	{
		Pushing = false;
	}
}

void AST_PushPullCharacter::Pull()
{

	if (!Pushing && !Pulling && bCan_Push_Pull)
	{
		StopPush();
		
		Pulling = true;
		ST_Direction = -1.0f;
		
	}
	if (Pushing)
	{
		//Pushing = false;
	}
}

void AST_PushPullCharacter::StopPull()
{
	

	if (Pulling)
	{
		Pulling = false;
	}
}


//**
void AST_PushPullCharacter::Interact()
{
	//**If near interactable object 
	if (IsInInteractionRangeOfObject)
	{
		if (!IsInteractingWithObject)
		{
			CanMove = false;
			IsInteractingWithObject = true;
			CharNeedsLocAndRotAdjustment = true;
			GetCharacterMovement()->bOrientRotationToMovement = false;
			GetCharacterMovement()->MaxWalkSpeed = 110.0f;

			APushable_Pullable_Cube* Cube = Cast<APushable_Pullable_Cube>(UGameplayStatics::GetActorOfClass(GetWorld(),
				APushable_Pullable_Cube::StaticClass()));

			Cube->CubeStaticMesh->SetSimulatePhysics(true);
			Cube->IsInteracting = true;
		}
		else
		{
			GetCharacterMovement()->bOrientRotationToMovement = true;
			GetCharacterMovement()->MaxWalkSpeed = 600.0f;
			CharNeedsLocAndRotAdjustment = false;
			IsInteractingWithObject = false;
			CanMove = true;
			bCan_Push_Pull = false;

			APushable_Pullable_Cube* Cube = Cast<APushable_Pullable_Cube>(UGameplayStatics::GetActorOfClass(GetWorld(),
				APushable_Pullable_Cube::StaticClass()));

			Cube->CubeStaticMesh->SetSimulatePhysics(false);
			Cube->IsInteracting = false;
		}
	}
}

//** Gets info from cube about FinalLocation and FinalRotation needed if Player desides to interract
void AST_PushPullCharacter::SetFinalCharLocationAndRotation(FVector HandleForwardLocation, FRotator HandleForwardRotation, FVector ForwardVector, FVector LocationCorrection)
{
	PushForward = ForwardVector;

	//*Takes Arrows X and Y Location  and char location.Z to make Target vector 
	float X = HandleForwardLocation.X;
	float Y = HandleForwardLocation.Y;
	float Z = HandleForwardLocation.Z;

	/*
	FString ValueX = FString::SanitizeFloat (X);
	FString ValueY = FString::SanitizeFloat(Y);
	FString ValueZ = FString::SanitizeFloat(Z);
	UKismetSystemLibrary::PrintString(GetWorld(), ValueX, true, true, FLinearColor::Blue, 2.0f);
	UKismetSystemLibrary::PrintString(GetWorld(), ValueY, true, true, FLinearColor::Blue, 2.0f);
	UKismetSystemLibrary::PrintString(GetWorld(), ValueZ, true, true, FLinearColor::Blue, 2.0f);
	*/

	CurrentCharLocation = GetActorLocation();
	FinalCharLocation = {X,Y,CurrentCharLocation.Z};
	
	//DrawDebugLine(GetWorld(),GetActorLocation(),FinalCharLocation, FColor::Red, true,5.0f);

	//**Animation offset
	LocationCorrection = ForwardVector * 20;
	FinalCharLocation -= LocationCorrection;

	//*Sets EndRotationVector to orient character for
	FRotator Rot = GetActorRotation();

	/*
	float Roll = HandleForwardRotation.Roll;
	float Pitch = HandleForwardRotation.Pitch;
	float Yaw = HandleForwardRotation.Yaw;

	FString ValueRoll = FString::SanitizeFloat(Roll);
	ValueRoll.Append("ArrowRoll");
	FString ValuePitch = FString::SanitizeFloat(Pitch);
	ValuePitch.Append("ArrowPitch");
	FString ValueYaw = FString::SanitizeFloat(Yaw);
	ValueYaw.Append("ArrowYaw");

	UKismetSystemLibrary::PrintString(GetWorld(), ValueRoll, true, true, FLinearColor::Blue, 2.0f);
	UKismetSystemLibrary::PrintString(GetWorld(), ValuePitch, true, true, FLinearColor::Blue, 2.0f);
	UKismetSystemLibrary::PrintString(GetWorld(), ValueYaw, true, true, FLinearColor::Blue, 2.0f);
	*/

	//**
	FinalCharRotation = { HandleForwardRotation };

	//**Charracter currently adjusting his location and rotatioin flag

}

//**Interpolates Char Location Vector  and Rotation Rotator
void AST_PushPullCharacter::InterpAlpha(float TargetAlpha, float DeltaTime)
{
	//**Interpolate Alpha 
	CurrentAlphaForLocAndRot = FMath::FInterpTo(0.0f, TargetAlpha, DeltaTime, 3.0f);

	//** Each tick interpolates CurrentActorLocation 
	FVector CharCurrentLocation = GetActorLocation();
	MyNewCharLocation = UKismetMathLibrary::VLerp(FinalCharLocation, CharCurrentLocation , CurrentAlphaForLocAndRot);

	
	FRotator CharCurrentRotation = GetActorRotation();
	/*
	float Roll = FinalCharRotation.Roll;
	float Pitch = FinalCharRotation.Pitch;
	float Yaw = FinalCharRotation.Yaw;
	FString ValueRoll = FString::SanitizeFloat(Roll);
	ValueRoll.Append("CharCurrentRotationRoll");

	FString ValuePitch = FString::SanitizeFloat(Pitch);
	ValuePitch.Append("CharCurrentRotationPitch");

	FString ValueYaw = FString::SanitizeFloat(Yaw);
	ValueYaw.Append("CharCurrentRotationYaw");

	UKismetSystemLibrary::PrintString(GetWorld(), ValueRoll, true, true, FLinearColor::Blue, 2.0f);
	UKismetSystemLibrary::PrintString(GetWorld(), ValuePitch, true, true, FLinearColor::Blue, 2.0f);
	UKismetSystemLibrary::PrintString(GetWorld(), ValueYaw, true, true, FLinearColor::Blue, 2.0f);
	*/
	MyNewCharRotation = UKismetMathLibrary::RLerp(FinalCharRotation, CharCurrentRotation, 0, true);

	//**
	RePositionTick(MyNewCharLocation, MyNewCharRotation);
}

//**Changes Characters location and rotation according to interpolated data
void AST_PushPullCharacter::RePositionTick( FVector NewCharLocation, FRotator NewCharRotation)
{
	SetActorLocationAndRotation(MyNewCharLocation, MyNewCharRotation, false, nullptr, ETeleportType::None);

	CharNeedsLocAndRotAdjustment = false;

	bCan_Push_Pull = true;
}


/*
bool AST_PushPullCharacter::CheckDistanceBetweenCharAndObj()
{
	//bool nearlyEqual = UKismetMathLibrary::NotEqual_VectorVector(CurrentCharLocation, FinalCharLocation, 500.0f);
	bool nearlyEqual = (CurrentCharLocation == FinalCharLocation && !FinalCharLocation.IsZero());

	if (nearlyEqual)
	{
		bCan_Push_Pull = true;
	}
	else
	{
		bCan_Push_Pull = false;
	}

	return nearlyEqual;
}
*/
