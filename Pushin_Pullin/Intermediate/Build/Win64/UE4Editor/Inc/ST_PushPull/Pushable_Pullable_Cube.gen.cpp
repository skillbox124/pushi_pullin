// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ST_PushPull/Pushable_Pullable_Cube.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePushable_Pullable_Cube() {}
// Cross Module References
	ST_PUSHPULL_API UFunction* Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_ST_PushPull();
	ST_PUSHPULL_API UClass* Z_Construct_UClass_APushable_Pullable_Cube_NoRegister();
	ST_PUSHPULL_API UClass* Z_Construct_UClass_APushable_Pullable_Cube();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UArrowComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics
	{
		struct _Script_ST_PushPull_eventOnActorInteractable_Parms
		{
			bool bIsInRage;
		};
		static void NewProp_bIsInRage_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsInRage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::NewProp_bIsInRage_SetBit(void* Obj)
	{
		((_Script_ST_PushPull_eventOnActorInteractable_Parms*)Obj)->bIsInRage = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::NewProp_bIsInRage = { "bIsInRage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_ST_PushPull_eventOnActorInteractable_Parms), &Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::NewProp_bIsInRage_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::NewProp_bIsInRage,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_ST_PushPull, nullptr, "OnActorInteractable__DelegateSignature", nullptr, nullptr, sizeof(_Script_ST_PushPull_eventOnActorInteractable_Parms), Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_ST_PushPull_OnActorInteractable__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(APushable_Pullable_Cube::execCollisionBoxYNEndOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CollisionBoxYNEndOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APushable_Pullable_Cube::execCollisionBoxYNBeginOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_GET_UBOOL(Z_Param_bFromSweep);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CollisionBoxYNBeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APushable_Pullable_Cube::execCollisionBoxYPEndOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CollisionBoxYPEndOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APushable_Pullable_Cube::execCollisionBoxYPBeginOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_GET_UBOOL(Z_Param_bFromSweep);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CollisionBoxYPBeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APushable_Pullable_Cube::execCollisionBoxXNEndOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CollisionBoxXNEndOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APushable_Pullable_Cube::execCollisionBoxXNBeginOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_GET_UBOOL(Z_Param_bFromSweep);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CollisionBoxXNBeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APushable_Pullable_Cube::execCollisionBoxXPEndOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComp);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CollisionBoxXPEndOverlap(Z_Param_OverlappedComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(APushable_Pullable_Cube::execCollisionBoxXPBeginOverlap)
	{
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent);
		P_GET_OBJECT(AActor,Z_Param_OtherActor);
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent);
		P_GET_PROPERTY(FIntProperty,Z_Param_OtherBodyIndex);
		P_GET_UBOOL(Z_Param_bFromSweep);
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CollisionBoxXPBeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult);
		P_NATIVE_END;
	}
	void APushable_Pullable_Cube::StaticRegisterNativesAPushable_Pullable_Cube()
	{
		UClass* Class = APushable_Pullable_Cube::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CollisionBoxXNBeginOverlap", &APushable_Pullable_Cube::execCollisionBoxXNBeginOverlap },
			{ "CollisionBoxXNEndOverlap", &APushable_Pullable_Cube::execCollisionBoxXNEndOverlap },
			{ "CollisionBoxXPBeginOverlap", &APushable_Pullable_Cube::execCollisionBoxXPBeginOverlap },
			{ "CollisionBoxXPEndOverlap", &APushable_Pullable_Cube::execCollisionBoxXPEndOverlap },
			{ "CollisionBoxYNBeginOverlap", &APushable_Pullable_Cube::execCollisionBoxYNBeginOverlap },
			{ "CollisionBoxYNEndOverlap", &APushable_Pullable_Cube::execCollisionBoxYNEndOverlap },
			{ "CollisionBoxYPBeginOverlap", &APushable_Pullable_Cube::execCollisionBoxYPBeginOverlap },
			{ "CollisionBoxYPEndOverlap", &APushable_Pullable_Cube::execCollisionBoxYPEndOverlap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics
	{
		struct Pushable_Pullable_Cube_eventCollisionBoxXNBeginOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComponent;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComponent;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OverlappedComponent = { "OverlappedComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXNBeginOverlap_Parms, OverlappedComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXNBeginOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OtherComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OtherComponent = { "OtherComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXNBeginOverlap_Parms, OtherComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OtherComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OtherComponent_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXNBeginOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((Pushable_Pullable_Cube_eventCollisionBoxXNBeginOverlap_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Pushable_Pullable_Cube_eventCollisionBoxXNBeginOverlap_Parms), &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXNBeginOverlap_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_SweepResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_SweepResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OverlappedComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OtherComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::NewProp_SweepResult,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APushable_Pullable_Cube, nullptr, "CollisionBoxXNBeginOverlap", nullptr, nullptr, sizeof(Pushable_Pullable_Cube_eventCollisionBoxXNBeginOverlap_Parms), Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics
	{
		struct Pushable_Pullable_Cube_eventCollisionBoxXNEndOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXNEndOverlap_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OverlappedComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OverlappedComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXNEndOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXNEndOverlap_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXNEndOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OverlappedComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::NewProp_OtherBodyIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APushable_Pullable_Cube, nullptr, "CollisionBoxXNEndOverlap", nullptr, nullptr, sizeof(Pushable_Pullable_Cube_eventCollisionBoxXNEndOverlap_Parms), Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics
	{
		struct Pushable_Pullable_Cube_eventCollisionBoxXPBeginOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComponent;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComponent;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OverlappedComponent = { "OverlappedComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXPBeginOverlap_Parms, OverlappedComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXPBeginOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OtherComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OtherComponent = { "OtherComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXPBeginOverlap_Parms, OtherComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OtherComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OtherComponent_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXPBeginOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((Pushable_Pullable_Cube_eventCollisionBoxXPBeginOverlap_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Pushable_Pullable_Cube_eventCollisionBoxXPBeginOverlap_Parms), &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXPBeginOverlap_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_SweepResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_SweepResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OverlappedComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OtherComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::NewProp_SweepResult,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APushable_Pullable_Cube, nullptr, "CollisionBoxXPBeginOverlap", nullptr, nullptr, sizeof(Pushable_Pullable_Cube_eventCollisionBoxXPBeginOverlap_Parms), Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics
	{
		struct Pushable_Pullable_Cube_eventCollisionBoxXPEndOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXPEndOverlap_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OverlappedComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OverlappedComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXPEndOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXPEndOverlap_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxXPEndOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OverlappedComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::NewProp_OtherBodyIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APushable_Pullable_Cube, nullptr, "CollisionBoxXPEndOverlap", nullptr, nullptr, sizeof(Pushable_Pullable_Cube_eventCollisionBoxXPEndOverlap_Parms), Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics
	{
		struct Pushable_Pullable_Cube_eventCollisionBoxYNBeginOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComponent;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComponent;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OverlappedComponent = { "OverlappedComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYNBeginOverlap_Parms, OverlappedComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYNBeginOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OtherComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OtherComponent = { "OtherComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYNBeginOverlap_Parms, OtherComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OtherComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OtherComponent_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYNBeginOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((Pushable_Pullable_Cube_eventCollisionBoxYNBeginOverlap_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Pushable_Pullable_Cube_eventCollisionBoxYNBeginOverlap_Parms), &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYNBeginOverlap_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_SweepResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_SweepResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OverlappedComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OtherComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::NewProp_SweepResult,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APushable_Pullable_Cube, nullptr, "CollisionBoxYNBeginOverlap", nullptr, nullptr, sizeof(Pushable_Pullable_Cube_eventCollisionBoxYNBeginOverlap_Parms), Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics
	{
		struct Pushable_Pullable_Cube_eventCollisionBoxYNEndOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYNEndOverlap_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OverlappedComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OverlappedComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYNEndOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYNEndOverlap_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYNEndOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OverlappedComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::NewProp_OtherBodyIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APushable_Pullable_Cube, nullptr, "CollisionBoxYNEndOverlap", nullptr, nullptr, sizeof(Pushable_Pullable_Cube_eventCollisionBoxYNEndOverlap_Parms), Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics
	{
		struct Pushable_Pullable_Cube_eventCollisionBoxYPBeginOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComponent;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComponent;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static void NewProp_bFromSweep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFromSweep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SweepResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SweepResult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OverlappedComponent = { "OverlappedComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYPBeginOverlap_Parms, OverlappedComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OverlappedComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYPBeginOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OtherComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OtherComponent = { "OtherComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYPBeginOverlap_Parms, OtherComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OtherComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OtherComponent_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYPBeginOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_bFromSweep_SetBit(void* Obj)
	{
		((Pushable_Pullable_Cube_eventCollisionBoxYPBeginOverlap_Parms*)Obj)->bFromSweep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_bFromSweep = { "bFromSweep", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(Pushable_Pullable_Cube_eventCollisionBoxYPBeginOverlap_Parms), &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_bFromSweep_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_SweepResult_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_SweepResult = { "SweepResult", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYPBeginOverlap_Parms, SweepResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_SweepResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_SweepResult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OverlappedComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OtherComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_OtherBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_bFromSweep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::NewProp_SweepResult,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APushable_Pullable_Cube, nullptr, "CollisionBoxYPBeginOverlap", nullptr, nullptr, sizeof(Pushable_Pullable_Cube_eventCollisionBoxYPBeginOverlap_Parms), Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics
	{
		struct Pushable_Pullable_Cube_eventCollisionBoxYPEndOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComp;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlappedComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappedComp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OtherComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OtherComp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OtherBodyIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OverlappedComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OverlappedComp = { "OverlappedComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYPEndOverlap_Parms, OverlappedComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OverlappedComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OverlappedComp_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OtherActor = { "OtherActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYPEndOverlap_Parms, OtherActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OtherComp_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OtherComp = { "OtherComp", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYPEndOverlap_Parms, OtherComp), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OtherComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OtherComp_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OtherBodyIndex = { "OtherBodyIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(Pushable_Pullable_Cube_eventCollisionBoxYPEndOverlap_Parms, OtherBodyIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OverlappedComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OtherActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OtherComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::NewProp_OtherBodyIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_APushable_Pullable_Cube, nullptr, "CollisionBoxYPEndOverlap", nullptr, nullptr, sizeof(Pushable_Pullable_Cube_eventCollisionBoxYPEndOverlap_Parms), Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APushable_Pullable_Cube_NoRegister()
	{
		return APushable_Pullable_Cube::StaticClass();
	}
	struct Z_Construct_UClass_APushable_Pullable_Cube_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CubeStaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CubeStaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Handle1_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Handle1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Handle2_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Handle2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Handle3_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Handle3;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Handle4_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Handle4;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionBoxXP_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionBoxXP;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionBoxXN_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionBoxXN;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionBoxYP_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionBoxYP;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionBoxYN_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionBoxYN;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Arrow1_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Arrow1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Arrow2_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Arrow2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Arrow3_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Arrow3;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Arrow4_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Arrow4;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HaveActorInInteractionZone_MetaData[];
#endif
		static void NewProp_HaveActorInInteractionZone_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_HaveActorInInteractionZone;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsInteracting_MetaData[];
#endif
		static void NewProp_IsInteracting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsInteracting;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APushable_Pullable_Cube_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ST_PushPull,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_APushable_Pullable_Cube_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNBeginOverlap, "CollisionBoxXNBeginOverlap" }, // 3953373821
		{ &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXNEndOverlap, "CollisionBoxXNEndOverlap" }, // 2234407580
		{ &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPBeginOverlap, "CollisionBoxXPBeginOverlap" }, // 319156156
		{ &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxXPEndOverlap, "CollisionBoxXPEndOverlap" }, // 4183578900
		{ &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNBeginOverlap, "CollisionBoxYNBeginOverlap" }, // 787089870
		{ &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYNEndOverlap, "CollisionBoxYNEndOverlap" }, // 762489686
		{ &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPBeginOverlap, "CollisionBoxYPBeginOverlap" }, // 1633237147
		{ &Z_Construct_UFunction_APushable_Pullable_Cube_CollisionBoxYPEndOverlap, "CollisionBoxYPEndOverlap" }, // 3410320880
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Pushable_Pullable_Cube.h" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CubeStaticMesh_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CubeStaticMesh = { "CubeStaticMesh", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, CubeStaticMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CubeStaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CubeStaticMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle1_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle1 = { "Handle1", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, Handle1), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle2_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle2 = { "Handle2", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, Handle2), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle3_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle3 = { "Handle3", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, Handle3), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle3_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle3_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle4_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle4 = { "Handle4", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, Handle4), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle4_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle4_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXP_MetaData[] = {
		{ "Category", "Components" },
		{ "Comment", "//\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXP = { "CollisionBoxXP", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, CollisionBoxXP), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXP_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXP_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXN_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXN = { "CollisionBoxXN", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, CollisionBoxXN), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXN_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXN_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYP_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYP = { "CollisionBoxYP", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, CollisionBoxYP), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYP_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYP_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYN_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYN = { "CollisionBoxYN", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, CollisionBoxYN), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYN_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYN_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow1_MetaData[] = {
		{ "Category", "Components" },
		{ "Comment", "//\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow1 = { "Arrow1", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, Arrow1), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow2_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow2 = { "Arrow2", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, Arrow2), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow3_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow3 = { "Arrow3", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, Arrow3), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow3_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow3_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow4_MetaData[] = {
		{ "Category", "Components" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow4 = { "Arrow4", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APushable_Pullable_Cube, Arrow4), Z_Construct_UClass_UArrowComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow4_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow4_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_HaveActorInInteractionZone_MetaData[] = {
		{ "Category", "Variables" },
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	void Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_HaveActorInInteractionZone_SetBit(void* Obj)
	{
		((APushable_Pullable_Cube*)Obj)->HaveActorInInteractionZone = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_HaveActorInInteractionZone = { "HaveActorInInteractionZone", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(APushable_Pullable_Cube), &Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_HaveActorInInteractionZone_SetBit, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_HaveActorInInteractionZone_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_HaveActorInInteractionZone_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_IsInteracting_MetaData[] = {
		{ "Category", "Variables" },
		{ "ModuleRelativePath", "Pushable_Pullable_Cube.h" },
	};
#endif
	void Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_IsInteracting_SetBit(void* Obj)
	{
		((APushable_Pullable_Cube*)Obj)->IsInteracting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_IsInteracting = { "IsInteracting", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(APushable_Pullable_Cube), &Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_IsInteracting_SetBit, METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_IsInteracting_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_IsInteracting_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APushable_Pullable_Cube_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CubeStaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle3,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Handle4,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXP,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxXN,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYP,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_CollisionBoxYN,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow3,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_Arrow4,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_HaveActorInInteractionZone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APushable_Pullable_Cube_Statics::NewProp_IsInteracting,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APushable_Pullable_Cube_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APushable_Pullable_Cube>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APushable_Pullable_Cube_Statics::ClassParams = {
		&APushable_Pullable_Cube::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_APushable_Pullable_Cube_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APushable_Pullable_Cube_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APushable_Pullable_Cube_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APushable_Pullable_Cube()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APushable_Pullable_Cube_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APushable_Pullable_Cube, 641260782);
	template<> ST_PUSHPULL_API UClass* StaticClass<APushable_Pullable_Cube>()
	{
		return APushable_Pullable_Cube::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APushable_Pullable_Cube(Z_Construct_UClass_APushable_Pullable_Cube, &APushable_Pullable_Cube::StaticClass, TEXT("/Script/ST_PushPull"), TEXT("APushable_Pullable_Cube"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APushable_Pullable_Cube);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
