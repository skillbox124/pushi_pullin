// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ST_PushPull/ST_PushPullCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeST_PushPullCharacter() {}
// Cross Module References
	ST_PUSHPULL_API UClass* Z_Construct_UClass_AST_PushPullCharacter_NoRegister();
	ST_PUSHPULL_API UClass* Z_Construct_UClass_AST_PushPullCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_ST_PushPull();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AST_PushPullCharacter::execInteract)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Interact();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AST_PushPullCharacter::execStopPull)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopPull();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AST_PushPullCharacter::execPull)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Pull();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AST_PushPullCharacter::execStopPush)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopPush();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AST_PushPullCharacter::execPush)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Push();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AST_PushPullCharacter::execMoveForward)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MoveForward(Z_Param_Value);
		P_NATIVE_END;
	}
	void AST_PushPullCharacter::StaticRegisterNativesAST_PushPullCharacter()
	{
		UClass* Class = AST_PushPullCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Interact", &AST_PushPullCharacter::execInteract },
			{ "MoveForward", &AST_PushPullCharacter::execMoveForward },
			{ "Pull", &AST_PushPullCharacter::execPull },
			{ "Push", &AST_PushPullCharacter::execPush },
			{ "StopPull", &AST_PushPullCharacter::execStopPull },
			{ "StopPush", &AST_PushPullCharacter::execStopPush },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AST_PushPullCharacter_Interact_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AST_PushPullCharacter_Interact_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//** Called to start interacting with objects\n" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
		{ "ToolTip", "/ Called to start interacting with objects" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AST_PushPullCharacter_Interact_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AST_PushPullCharacter, nullptr, "Interact", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AST_PushPullCharacter_Interact_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AST_PushPullCharacter_Interact_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AST_PushPullCharacter_Interact()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AST_PushPullCharacter_Interact_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics
	{
		struct ST_PushPullCharacter_eventMoveForward_Parms
		{
			float Value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ST_PushPullCharacter_eventMoveForward_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Called for forwards/backward input */" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
		{ "ToolTip", "Called for forwards/backward input" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AST_PushPullCharacter, nullptr, "MoveForward", nullptr, nullptr, sizeof(ST_PushPullCharacter_eventMoveForward_Parms), Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AST_PushPullCharacter_MoveForward()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AST_PushPullCharacter_MoveForward_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AST_PushPullCharacter_Pull_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AST_PushPullCharacter_Pull_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AST_PushPullCharacter_Pull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AST_PushPullCharacter, nullptr, "Pull", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AST_PushPullCharacter_Pull_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AST_PushPullCharacter_Pull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AST_PushPullCharacter_Pull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AST_PushPullCharacter_Pull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AST_PushPullCharacter_Push_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AST_PushPullCharacter_Push_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//** Called for Moving objects\n" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
		{ "ToolTip", "/ Called for Moving objects" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AST_PushPullCharacter_Push_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AST_PushPullCharacter, nullptr, "Push", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AST_PushPullCharacter_Push_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AST_PushPullCharacter_Push_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AST_PushPullCharacter_Push()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AST_PushPullCharacter_Push_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AST_PushPullCharacter_StopPull_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AST_PushPullCharacter_StopPull_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AST_PushPullCharacter_StopPull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AST_PushPullCharacter, nullptr, "StopPull", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AST_PushPullCharacter_StopPull_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AST_PushPullCharacter_StopPull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AST_PushPullCharacter_StopPull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AST_PushPullCharacter_StopPull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AST_PushPullCharacter_StopPush_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AST_PushPullCharacter_StopPush_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AST_PushPullCharacter_StopPush_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AST_PushPullCharacter, nullptr, "StopPush", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AST_PushPullCharacter_StopPush_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AST_PushPullCharacter_StopPush_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AST_PushPullCharacter_StopPush()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AST_PushPullCharacter_StopPush_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AST_PushPullCharacter_NoRegister()
	{
		return AST_PushPullCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AST_PushPullCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FollowCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FollowCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseTurnRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseTurnRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseLookUpRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BaseLookUpRate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ST_Direction_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ST_Direction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CanMove_MetaData[];
#endif
		static void NewProp_CanMove_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_CanMove;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsInInteractionRangeOfObject_MetaData[];
#endif
		static void NewProp_IsInInteractionRangeOfObject_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsInInteractionRangeOfObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsInteractingWithObject_MetaData[];
#endif
		static void NewProp_IsInteractingWithObject_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsInteractingWithObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCan_Push_Pull_MetaData[];
#endif
		static void NewProp_bCan_Push_Pull_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCan_Push_Pull;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pushing_MetaData[];
#endif
		static void NewProp_Pushing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Pushing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pulling_MetaData[];
#endif
		static void NewProp_Pulling_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Pulling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CharNeedsLocAndRotAdjustment_MetaData[];
#endif
		static void NewProp_CharNeedsLocAndRotAdjustment_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_CharNeedsLocAndRotAdjustment;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AST_PushPullCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_ST_PushPull,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AST_PushPullCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AST_PushPullCharacter_Interact, "Interact" }, // 2644270879
		{ &Z_Construct_UFunction_AST_PushPullCharacter_MoveForward, "MoveForward" }, // 735141551
		{ &Z_Construct_UFunction_AST_PushPullCharacter_Pull, "Pull" }, // 2277977284
		{ &Z_Construct_UFunction_AST_PushPullCharacter_Push, "Push" }, // 2128251236
		{ &Z_Construct_UFunction_AST_PushPullCharacter_StopPull, "StopPull" }, // 1413769649
		{ &Z_Construct_UFunction_AST_PushPullCharacter_StopPush, "StopPush" }, // 4283225960
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "ST_PushPullCharacter.h" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera behind the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera behind the character" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AST_PushPullCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CameraBoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_FollowCamera_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Follow camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
		{ "ToolTip", "Follow camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_FollowCamera = { "FollowCamera", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AST_PushPullCharacter, FollowCamera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_FollowCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_FollowCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseTurnRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "Comment", "/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseTurnRate = { "BaseTurnRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AST_PushPullCharacter, BaseTurnRate), METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseTurnRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseTurnRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseLookUpRate_MetaData[] = {
		{ "Category", "Camera" },
		{ "Comment", "/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
		{ "ToolTip", "Base look up/down rate, in deg/sec. Other scaling may affect final rate." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseLookUpRate = { "BaseLookUpRate", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AST_PushPullCharacter, BaseLookUpRate), METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseLookUpRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseLookUpRate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_ST_Direction_MetaData[] = {
		{ "Category", "ST_PushPullCharacter" },
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_ST_Direction = { "ST_Direction", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AST_PushPullCharacter, ST_Direction), METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_ST_Direction_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_ST_Direction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CanMove_MetaData[] = {
		{ "Category", "ST_PushPullCharacter" },
		{ "Comment", "//** \n" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CanMove_SetBit(void* Obj)
	{
		((AST_PushPullCharacter*)Obj)->CanMove = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CanMove = { "CanMove", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AST_PushPullCharacter), &Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CanMove_SetBit, METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CanMove_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CanMove_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInInteractionRangeOfObject_MetaData[] = {
		{ "Category", "ST_PushPullCharacter" },
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInInteractionRangeOfObject_SetBit(void* Obj)
	{
		((AST_PushPullCharacter*)Obj)->IsInInteractionRangeOfObject = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInInteractionRangeOfObject = { "IsInInteractionRangeOfObject", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AST_PushPullCharacter), &Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInInteractionRangeOfObject_SetBit, METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInInteractionRangeOfObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInInteractionRangeOfObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInteractingWithObject_MetaData[] = {
		{ "Category", "ST_PushPullCharacter" },
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInteractingWithObject_SetBit(void* Obj)
	{
		((AST_PushPullCharacter*)Obj)->IsInteractingWithObject = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInteractingWithObject = { "IsInteractingWithObject", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AST_PushPullCharacter), &Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInteractingWithObject_SetBit, METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInteractingWithObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInteractingWithObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_bCan_Push_Pull_MetaData[] = {
		{ "Category", "ST_PushPullCharacter" },
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_bCan_Push_Pull_SetBit(void* Obj)
	{
		((AST_PushPullCharacter*)Obj)->bCan_Push_Pull = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_bCan_Push_Pull = { "bCan_Push_Pull", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AST_PushPullCharacter), &Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_bCan_Push_Pull_SetBit, METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_bCan_Push_Pull_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_bCan_Push_Pull_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pushing_MetaData[] = {
		{ "Category", "ST_PushPullCharacter" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pushing_SetBit(void* Obj)
	{
		((AST_PushPullCharacter*)Obj)->Pushing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pushing = { "Pushing", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AST_PushPullCharacter), &Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pushing_SetBit, METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pushing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pushing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pulling_MetaData[] = {
		{ "Category", "ST_PushPullCharacter" },
		{ "Comment", "//**\n" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pulling_SetBit(void* Obj)
	{
		((AST_PushPullCharacter*)Obj)->Pulling = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pulling = { "Pulling", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AST_PushPullCharacter), &Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pulling_SetBit, METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pulling_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pulling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CharNeedsLocAndRotAdjustment_MetaData[] = {
		{ "Category", "ST_PushPullCharacter" },
		{ "ModuleRelativePath", "ST_PushPullCharacter.h" },
	};
#endif
	void Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CharNeedsLocAndRotAdjustment_SetBit(void* Obj)
	{
		((AST_PushPullCharacter*)Obj)->CharNeedsLocAndRotAdjustment = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CharNeedsLocAndRotAdjustment = { "CharNeedsLocAndRotAdjustment", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AST_PushPullCharacter), &Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CharNeedsLocAndRotAdjustment_SetBit, METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CharNeedsLocAndRotAdjustment_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CharNeedsLocAndRotAdjustment_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AST_PushPullCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CameraBoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_FollowCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseTurnRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_BaseLookUpRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_ST_Direction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CanMove,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInInteractionRangeOfObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_IsInteractingWithObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_bCan_Push_Pull,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pushing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_Pulling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AST_PushPullCharacter_Statics::NewProp_CharNeedsLocAndRotAdjustment,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AST_PushPullCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AST_PushPullCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AST_PushPullCharacter_Statics::ClassParams = {
		&AST_PushPullCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AST_PushPullCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AST_PushPullCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AST_PushPullCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AST_PushPullCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AST_PushPullCharacter, 3053783970);
	template<> ST_PUSHPULL_API UClass* StaticClass<AST_PushPullCharacter>()
	{
		return AST_PushPullCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AST_PushPullCharacter(Z_Construct_UClass_AST_PushPullCharacter, &AST_PushPullCharacter::StaticClass, TEXT("/Script/ST_PushPull"), TEXT("AST_PushPullCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AST_PushPullCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
