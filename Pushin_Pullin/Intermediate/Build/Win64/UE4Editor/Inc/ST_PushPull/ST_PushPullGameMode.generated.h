// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ST_PUSHPULL_ST_PushPullGameMode_generated_h
#error "ST_PushPullGameMode.generated.h already included, missing '#pragma once' in ST_PushPullGameMode.h"
#endif
#define ST_PUSHPULL_ST_PushPullGameMode_generated_h

#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_SPARSE_DATA
#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_RPC_WRAPPERS
#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAST_PushPullGameMode(); \
	friend struct Z_Construct_UClass_AST_PushPullGameMode_Statics; \
public: \
	DECLARE_CLASS(AST_PushPullGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ST_PushPull"), ST_PUSHPULL_API) \
	DECLARE_SERIALIZER(AST_PushPullGameMode)


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAST_PushPullGameMode(); \
	friend struct Z_Construct_UClass_AST_PushPullGameMode_Statics; \
public: \
	DECLARE_CLASS(AST_PushPullGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ST_PushPull"), ST_PUSHPULL_API) \
	DECLARE_SERIALIZER(AST_PushPullGameMode)


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ST_PUSHPULL_API AST_PushPullGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AST_PushPullGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ST_PUSHPULL_API, AST_PushPullGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AST_PushPullGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ST_PUSHPULL_API AST_PushPullGameMode(AST_PushPullGameMode&&); \
	ST_PUSHPULL_API AST_PushPullGameMode(const AST_PushPullGameMode&); \
public:


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ST_PUSHPULL_API AST_PushPullGameMode(AST_PushPullGameMode&&); \
	ST_PUSHPULL_API AST_PushPullGameMode(const AST_PushPullGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ST_PUSHPULL_API, AST_PushPullGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AST_PushPullGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AST_PushPullGameMode)


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_9_PROLOG
#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_SPARSE_DATA \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_RPC_WRAPPERS \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_INCLASS \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_SPARSE_DATA \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_INCLASS_NO_PURE_DECLS \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ST_PUSHPULL_API UClass* StaticClass<class AST_PushPullGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pushin_Pullin_Source_ST_PushPull_ST_PushPullGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
