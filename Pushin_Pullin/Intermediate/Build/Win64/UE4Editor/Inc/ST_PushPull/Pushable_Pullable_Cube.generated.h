// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef ST_PUSHPULL_Pushable_Pullable_Cube_generated_h
#error "Pushable_Pullable_Cube.generated.h already included, missing '#pragma once' in Pushable_Pullable_Cube.h"
#endif
#define ST_PUSHPULL_Pushable_Pullable_Cube_generated_h

#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_15_DELEGATE \
struct _Script_ST_PushPull_eventOnActorInteractable_Parms \
{ \
	bool bIsInRage; \
}; \
static inline void FOnActorInteractable_DelegateWrapper(const FMulticastScriptDelegate& OnActorInteractable, bool bIsInRage) \
{ \
	_Script_ST_PushPull_eventOnActorInteractable_Parms Parms; \
	Parms.bIsInRage=bIsInRage ? true : false; \
	OnActorInteractable.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_SPARSE_DATA
#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCollisionBoxYNEndOverlap); \
	DECLARE_FUNCTION(execCollisionBoxYNBeginOverlap); \
	DECLARE_FUNCTION(execCollisionBoxYPEndOverlap); \
	DECLARE_FUNCTION(execCollisionBoxYPBeginOverlap); \
	DECLARE_FUNCTION(execCollisionBoxXNEndOverlap); \
	DECLARE_FUNCTION(execCollisionBoxXNBeginOverlap); \
	DECLARE_FUNCTION(execCollisionBoxXPEndOverlap); \
	DECLARE_FUNCTION(execCollisionBoxXPBeginOverlap);


#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCollisionBoxYNEndOverlap); \
	DECLARE_FUNCTION(execCollisionBoxYNBeginOverlap); \
	DECLARE_FUNCTION(execCollisionBoxYPEndOverlap); \
	DECLARE_FUNCTION(execCollisionBoxYPBeginOverlap); \
	DECLARE_FUNCTION(execCollisionBoxXNEndOverlap); \
	DECLARE_FUNCTION(execCollisionBoxXNBeginOverlap); \
	DECLARE_FUNCTION(execCollisionBoxXPEndOverlap); \
	DECLARE_FUNCTION(execCollisionBoxXPBeginOverlap);


#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPushable_Pullable_Cube(); \
	friend struct Z_Construct_UClass_APushable_Pullable_Cube_Statics; \
public: \
	DECLARE_CLASS(APushable_Pullable_Cube, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ST_PushPull"), NO_API) \
	DECLARE_SERIALIZER(APushable_Pullable_Cube)


#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAPushable_Pullable_Cube(); \
	friend struct Z_Construct_UClass_APushable_Pullable_Cube_Statics; \
public: \
	DECLARE_CLASS(APushable_Pullable_Cube, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ST_PushPull"), NO_API) \
	DECLARE_SERIALIZER(APushable_Pullable_Cube)


#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APushable_Pullable_Cube(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APushable_Pullable_Cube) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APushable_Pullable_Cube); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APushable_Pullable_Cube); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APushable_Pullable_Cube(APushable_Pullable_Cube&&); \
	NO_API APushable_Pullable_Cube(const APushable_Pullable_Cube&); \
public:


#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APushable_Pullable_Cube(APushable_Pullable_Cube&&); \
	NO_API APushable_Pullable_Cube(const APushable_Pullable_Cube&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APushable_Pullable_Cube); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APushable_Pullable_Cube); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APushable_Pullable_Cube)


#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_PRIVATE_PROPERTY_OFFSET
#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_17_PROLOG
#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_PRIVATE_PROPERTY_OFFSET \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_SPARSE_DATA \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_RPC_WRAPPERS \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_INCLASS \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_PRIVATE_PROPERTY_OFFSET \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_SPARSE_DATA \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_INCLASS_NO_PURE_DECLS \
	Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ST_PUSHPULL_API UClass* StaticClass<class APushable_Pullable_Cube>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pushin_Pullin_Source_ST_PushPull_Pushable_Pullable_Cube_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
