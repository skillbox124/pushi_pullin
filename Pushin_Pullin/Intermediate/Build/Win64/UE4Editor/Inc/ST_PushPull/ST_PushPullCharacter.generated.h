// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ST_PUSHPULL_ST_PushPullCharacter_generated_h
#error "ST_PushPullCharacter.generated.h already included, missing '#pragma once' in ST_PushPullCharacter.h"
#endif
#define ST_PUSHPULL_ST_PushPullCharacter_generated_h

#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_SPARSE_DATA
#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execInteract); \
	DECLARE_FUNCTION(execStopPull); \
	DECLARE_FUNCTION(execPull); \
	DECLARE_FUNCTION(execStopPush); \
	DECLARE_FUNCTION(execPush); \
	DECLARE_FUNCTION(execMoveForward);


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execInteract); \
	DECLARE_FUNCTION(execStopPull); \
	DECLARE_FUNCTION(execPull); \
	DECLARE_FUNCTION(execStopPush); \
	DECLARE_FUNCTION(execPush); \
	DECLARE_FUNCTION(execMoveForward);


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAST_PushPullCharacter(); \
	friend struct Z_Construct_UClass_AST_PushPullCharacter_Statics; \
public: \
	DECLARE_CLASS(AST_PushPullCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ST_PushPull"), NO_API) \
	DECLARE_SERIALIZER(AST_PushPullCharacter)


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAST_PushPullCharacter(); \
	friend struct Z_Construct_UClass_AST_PushPullCharacter_Statics; \
public: \
	DECLARE_CLASS(AST_PushPullCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ST_PushPull"), NO_API) \
	DECLARE_SERIALIZER(AST_PushPullCharacter)


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AST_PushPullCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AST_PushPullCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AST_PushPullCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AST_PushPullCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AST_PushPullCharacter(AST_PushPullCharacter&&); \
	NO_API AST_PushPullCharacter(const AST_PushPullCharacter&); \
public:


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AST_PushPullCharacter(AST_PushPullCharacter&&); \
	NO_API AST_PushPullCharacter(const AST_PushPullCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AST_PushPullCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AST_PushPullCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AST_PushPullCharacter)


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AST_PushPullCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AST_PushPullCharacter, FollowCamera); }


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_15_PROLOG
#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_SPARSE_DATA \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_RPC_WRAPPERS \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_INCLASS \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_PRIVATE_PROPERTY_OFFSET \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_SPARSE_DATA \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_INCLASS_NO_PURE_DECLS \
	Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ST_PUSHPULL_API UClass* StaticClass<class AST_PushPullCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pushin_Pullin_Source_ST_PushPull_ST_PushPullCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
