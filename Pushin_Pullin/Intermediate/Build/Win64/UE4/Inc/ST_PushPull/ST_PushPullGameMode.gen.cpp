// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ST_PushPull/ST_PushPullGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeST_PushPullGameMode() {}
// Cross Module References
	ST_PUSHPULL_API UClass* Z_Construct_UClass_AST_PushPullGameMode_NoRegister();
	ST_PUSHPULL_API UClass* Z_Construct_UClass_AST_PushPullGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_ST_PushPull();
// End Cross Module References
	void AST_PushPullGameMode::StaticRegisterNativesAST_PushPullGameMode()
	{
	}
	UClass* Z_Construct_UClass_AST_PushPullGameMode_NoRegister()
	{
		return AST_PushPullGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AST_PushPullGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AST_PushPullGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ST_PushPull,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AST_PushPullGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "ST_PushPullGameMode.h" },
		{ "ModuleRelativePath", "ST_PushPullGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AST_PushPullGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AST_PushPullGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AST_PushPullGameMode_Statics::ClassParams = {
		&AST_PushPullGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AST_PushPullGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AST_PushPullGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AST_PushPullGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AST_PushPullGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AST_PushPullGameMode, 399314256);
	template<> ST_PUSHPULL_API UClass* StaticClass<AST_PushPullGameMode>()
	{
		return AST_PushPullGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AST_PushPullGameMode(Z_Construct_UClass_AST_PushPullGameMode, &AST_PushPullGameMode::StaticClass, TEXT("/Script/ST_PushPull"), TEXT("AST_PushPullGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AST_PushPullGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
