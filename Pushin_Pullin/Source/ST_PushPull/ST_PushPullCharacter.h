// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Math/UnrealMathUtility.h"
#include "Math/UnrealMathVectorCommon.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "../ST_PushPull/Pushable_Pullable_Cube.h"

#include "ST_PushPullCharacter.generated.h"

UCLASS(config=Game)
class AST_PushPullCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AST_PushPullCharacter();

	virtual void Tick(float Deltatime) override;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	//**
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float ST_Direction = 0.0f;
	

	//** 
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool CanMove = true;


	//**
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool IsInInteractionRangeOfObject = false;


	//**
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool IsInteractingWithObject = false;

	//**
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bCan_Push_Pull = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool Pushing = false;

	//**
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool Pulling = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool CharNeedsLocAndRotAdjustment = false;

	FVector PushForward = FVector(0.0f, 0.0f, 0.0f);
	//**
	FVector CurrentCharLocation = { 0.0f,0.0f,0.0f };
	FVector FinalCharLocation = { 0.0f,0.0f,0.0f };
	FVector MyNewCharLocation = { 0.0f,0.0f,0.0f };

	FRotator CurrentCharRotation = { 0.0f,0.0f,0.0f };
	FRotator FinalCharRotation = { 0.0f,0.0f,0.0f };
	FRotator MyNewCharRotation = { 0.0f,0.0f,0.0f };

	float TargetAlphaForLocAndRot = 0.3f;
	float CurrentAlphaForLocAndRot = 0.0f;

protected:

	/** Resets HMD orientation in VR. */
	//void OnResetVR();

	/** Called for forwards/backward input */
	UFUNCTION(BLueprintCallable)
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	//void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	//void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	//** Called for Moving objects
	UFUNCTION(BLueprintCallable)
		void Push();
	UFUNCTION(BLueprintCallable)
		void StopPush();

	UFUNCTION(BLueprintCallable)
		void Pull();
	UFUNCTION(BLueprintCallable)
		void StopPull();

	//** Called to start interacting with objects
	UFUNCTION(BLueprintCallable)
		void Interact();


	//**
	void InterpAlpha(float TargetAlpha,float Deltatime);

	//**
	void RePositionTick(FVector NewCharLocation, FRotator NewCharRotation);

	//**
	void SetFinalCharLocationAndRotation(FVector HandleForwardLocation, FRotator HandleForwardRotation, FVector ForwardVector, FVector LocationCorrection);

	//bool CheckDistanceBetweenCharAndObj();
};

